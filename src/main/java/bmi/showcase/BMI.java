package bmi.showcase;

import java.util.Locale;
import java.util.Scanner;

public class BMI {
    public static void main(String[] args) {
        double weight = weightInput();
        double height = heightInput();
        double bmi = bmiCalculation(weight, height);
        bmiConclusion(bmi);
    }

    public static double weightInput() {
        System.out.print("Please enter your weight in KG (e.g. '75.25'): ");
        return terminalScanner();
    }

    public static double heightInput() {
        System.out.print("Please enter your height in CM (e.g. '180'): ");
        return terminalScanner();
    }

    public static double terminalScanner() {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        return scanner.nextDouble();
    }

    public static double bmiCalculation(double weight, double height) {
        double bmi = weight / ((height / 100) * (height / 100));
        bmi = Math.round(bmi * 100.0) / 100.0;
        System.out.println("Your BMI is: " + bmi);
        return bmi;
    }

    public static void bmiConclusion(double bmi) {
        String conclusion;
        if (bmi < 18.5){
            conclusion = "underweight";
        }
        else if (bmi < 25){
            conclusion ="normal";
        }
        else if (bmi < 30){
            conclusion ="overweight";
        }
        else {
            conclusion = "obese";
        }
        System.out.print("Your BMI is considered: " + conclusion);
    }
}