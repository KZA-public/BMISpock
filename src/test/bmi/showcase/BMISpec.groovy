package bmi.showcase

import spock.lang.Specification
import spock.lang.Unroll

class BMISpec extends Specification{

    @Unroll
    "Weight input '#weightInput' should return '#expectedWeight'"() {
        given:
        BMI bmi = new BMI()

        when:
        System.setIn(new ByteArrayInputStream(weightInput.getBytes()))

        then:
        assert  bmi.weightInput() == expectedWeight

        where:
        weightInput | expectedWeight
        "70"        | 70
        "71.5"      | 71.50
    }

    @Unroll
    "Height input '#heightInput' should return '#expectedHeight'"() {
        given:
        BMI bmi = new BMI()

        when:
        System.setIn(new ByteArrayInputStream(heightInput.getBytes()))

        then:
        assert  bmi.heightInput() == expectedHeight

        where:
        heightInput | expectedHeight
        "70"        | 70
        "71.5"      | 71.50
    }

    @Unroll
    "Terminal scanner should read input '#terminalInput' and return '#expectedOutput'"() {
        given:
        BMI bmi = new BMI()

        when:
        System.setIn(new ByteArrayInputStream(terminalInput.getBytes()))

        then:
        assert bmi.terminalScanner() == expectedOutput

        where:
        terminalInput   | expectedOutput
        "70"            | 70
        "71.5"          | 71.50
        "71.75"         | 71.75
    }

    @Unroll
    "Terminal scanner should return an error on input '#terminalInput'"(){
        given:
        BMI bmi = new BMI()

        when:
        System.setIn(new ByteArrayInputStream(terminalInput.getBytes()))
        bmi.terminalScanner()

        then:
        thrown(expectedError)

        where:
        terminalInput   | expectedError
        "Text"          | InputMismatchException
        "/"             | InputMismatchException
        "75,50"         | InputMismatchException
        " "             | NoSuchElementException
        null            | NullPointerException
    }

    @Unroll
    "BMI should return '#expectedBMI' when the weight is '#weight' and height is '#height'"() {
        given:
        BMI bmi = new BMI()

        when:
        double calculatedBMI = bmi.bmiCalculation(weight,height)

        then:
        assert calculatedBMI == expectedBMI

        where:
        weight  | height | expectedBMI
        50      | 170    | 17.3
        60      | 160    | 23.44
        70      | 190    | 19.39
    }

    @Unroll
    "BMI of '#bodymassindex' should print '#expectedConclusion'"() {
        given:
        BMI bmi = new BMI()
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outContent))

        when:
        bmi.bmiConclusion(bodymassindex as double)

        then:
        assert outContent.toString() == "Your BMI is considered: " + expectedConclusion

        where:
        bodymassindex | expectedConclusion
        15            | "underweight"
        18.49         | "underweight"
        18.5          | "normal"
        24.99         | "normal"
        25            | "overweight"
        29.99         | "overweight"
        30            | "obese"
    }
}